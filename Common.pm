package Common;

use LWP;
use HTTP::Cookies;
use HTTP::Request::Common;
use strict;
require Exporter;
our @ISA = ("Exporter");
our @EXPORT = qw($browser @web_headers);


my $browser = LWP::UserAgent->new;
$browser->cookie_jar({});
$browser->proxy(http => 'http://192.168.0.10:8000');

my @web_headers = (
'User-Agent' => 'Opera/6.03 (Linux 2.4.19-gentoo-r7 i686; U)  [en]',
'Accept-Language' => 'en',
'Accept-Charset' => 'windows-1252;q=1.0, utf-8;q=1.0, utf-16;q=1.0, iso-8859-1;q=0.6, *;q=0.1',
'Accept-Encoding' => 'gzip',
);





sub do_POST {
    $browser = LWP::UserAgent->new() unless $browser;
    my $resp = $browser->post(@_, @web_headers);
    return ($resp->content, $resp->status_line, $resp->is_success, $resp) if wantarray;
    return unless $resp->is_success;
    return $resp->content;
}

sub do_GET {
    my $browser = LWP::UserAgent->new() unless $browser;
    my $resp = $browser->get(@_, @web_headers);
    return ($resp->content, $resp->status_line, $resp->is_success, $resp) if wantarray;
    return unless $resp->is_success;
    return $resp->content;
}
    
