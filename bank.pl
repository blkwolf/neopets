#!/usr/bin/perl -w

use LWP;
use HTTP::Cookies;
use HTTP::Request::Common;

$Date=`date +%m%d%H%M`;
chomp ($Date);

my $Username = 'oldusername';
my $Passwd = 'S0meo1dP@assword';


$browser = LWP::UserAgent->new;
$browser->cookie_jar({});
# $browser->proxy(http => 'http://192.168.0.10:8000');

my @web_headers = (
'User-Agent' => 'Opera/6.03 (Linux 2.4.19-gentoo-r7 i686; U)  [en]',
'Accept-Language' => 'en',
'Accept-Charset' => 'windows-1252;q=1.0, utf-8;q=1.0, utf-16;q=1.0, iso-8859-1;q=0.6, *;q=0.1',
'Accept-Encoding' => 'gzip',
);
 
my @bank_headers = (
'User-Agent' => 'Opera/6.03 (Linux 2.4.19-gentoo-r7 i686; U)  [en]',
'Accept-Language' => 'en',
'Accept-Charset' => 'windows-1252;q=1.0, utf-8;q=1.0, utf-16;q=1.0, iso-8859-1;q=0.6, *;q=0.1',
'Accept-Encoding' => 'gzip',
'Referer' => 'http://www.neopets.com/bank.phtml'
);

my ($content, $message, $is_success) = do_POST(
    'http://www.neopets.com/login.phtml',
    [ destination => '/petcentral.phtml',
      username => "$Username",
      password => "$Passwd" ],
);



Bank();



sub Bank {
    $bank = 'http://www.neopets.com/bank.phtml';
    my $response = $browser->get($bank, @web_headers);
    $html = $response->content;
    # $html =~ /<input type\=\'submit\' value\=\'(Grab some Omelette)\'>/;
    $html =~ /<input type\=\'submit\' value\=\'(Collect \d+ NP of Interest!)\'>/;
    $bank_OK = $1;
    if ($bank_OK) {
        my ($content, $message, $is_success) = do_POST_bank(
        'http://www.neopets.com/process_bank.phtml',
        ['type' => "interest" ],
	# print "Bank is OK!\n";
      );
   }
}


sub do_POST {
    $browser = LWP::UserAgent->new() unless $browser;
    my $resp = $browser->post(@_, @web_headers);
    return ($resp->content, $resp->status_line, $resp->is_success, $resp) if wantarray;
    return unless $resp->is_success;
    return $resp->content;
}

sub do_POST_bank {
    $browser = LWP::UserAgent->new() unless $browser;
    my $resp = $browser->post(@_, @bank_headers);
    return ($resp->content, $resp->status_line, $resp->is_success, $resp) if wantarray;
    return unless $resp->is_success;
    return $resp->content;
}
