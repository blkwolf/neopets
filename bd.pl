#!/usr/bin/perl -w
# battledome script to fight punchbag bob

use LWP;
use HTTP::Cookies;
use HTTP::Request::Common qw(POST);

my $Username = 'oldusername';
my $Passwd = 'S0meo1dP@assword';
my $BD = 'http://www.neopets.com/battledome/fight.phtml?fightid=43950796&pet1_name=Vuldrak';

$browser = LWP::UserAgent->new;
$browser->agent('Mozilla/4.76 [en] (Win98; U)');
$browser->cookie_jar({});
$browser->proxy(http => 'http://192.168.0.10:8000');
 

my ($content, $message, $is_success) = do_POST(
    'http://www.neopets.com/login.phtml',
    [ destination => '/petcentral.phtml',
      username => "$Username",
      password => "$Passwd" ],
    );

#$quik_pref = 'http://www.neopets.com/quickref.phtml';
#my $response = $browser->get($quik_pref);
#print $response->content;


Battle();

## SubRoutines ##

sub Battle {
    Fight_Status();
    
    while ($Bob > 1) {
      Fight_Bob();
      Fight_Status();
      $delay = int( rand(25));
      sleep $delay;
      
    }
}

sub Fight_Status {
    my $response= $browser->get($BD);
    $BDPage = $response->content;

    ## Get changing variables
    $fight_step = $BDPage =~ /<input type\=\'hidden\' name\=\'fight_step\' value\=\'(\d+)\'>/;
    $fight_step = $1;
    $fight_rnd = $BDPage =~ /<input type\=\'hidden\' name\=\'rnd\' value=\'(\d+)\'>/;
    $fight_rnd = $1;

    $Bob = $BDPage =~ /<b>Punchbag Bob<\/b><br><font color\=\'green\'><b>(\d+) \/ 5000<\/b>/;
    $Bob = $1;
    print "Bobs Health = $Bob of 5000\n";

}

sub Fight_Bob {
  my $response = $browser->post(
  'http://www.neopets.com/battledome/process_fight_oneplayer.phtml',
     [ selectmove => "abil_10039",
       pet1_name => "Vuldrak",
       pet2_name => "Punchbag+Bob",
       fight_id => "43950796",
       fight_step => "$fight_step",
       newchallenge => "false",
       tfx => "$fight_step",
       rnd => "$fight_rnd"],      
       );
}

sub do_POST {
    $browser = LWP::UserAgent->new() unless $browser;
    my $resp = $browser->post(@_);
    return ($resp->content, $resp->status_line, $resp->is_success, $resp) if wantarray;
    return unless $resp->is_success;
    return $resp->content;
}

