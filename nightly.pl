#!/usr/bin/perl -w
#
use LWP;
use HTTP::Cookies;
use HTTP::Request::Common;

$Date=`date +%m%d%H%M`;
chomp ($Date);

my $Username = 'oldusername';
my $Passwd = 'S0meo1dP@assword';

$browser = LWP::UserAgent->new;
$browser->cookie_jar({});
$browser->proxy(http => 'http://192.168.0.10:8000');

my @web_headers = (
'User-Agent' => 'Opera/6.03 (Linux 2.4.19-gentoo-r7 i686; U)  [en]',
'Accept-Language' => 'en',
'Accept-Charset' => 'windows-1252;q=1.0, utf-8;q=1.0, utf-16;q=1.0, iso-8859-1;q=0.6, *;q=0.1',
'Accept-Encoding' => 'gzip',
);
 

my ($content, $message, $is_success) = do_POST(
    'http://www.neopets.com/login.phtml',
    [ destination => '/petcentral.phtml',
      username => "$Username",
      password => "$Passwd" ],
    );

#$quik_pref = 'http://www.neopets.com/quickref.phtml';
#my $response = $browser->get($quik_pref);
#print $response->content;

the_Shrine();
get_Omlette();
Fruit_Machine();
#Tombola();

    
sub Fruit_Machine {
   $fmachine =  'http://www.neopets.com/desert/fruitmachine2.phtml';
    my $response = $browser->get($fmachine, @web_headers);
    open(OUT, ">/home/oldusername/neopets/fruit-$Date.html") || die $!;
    print OUT $response->content;
    close(OUT);

}

sub get_Omlette {
    $omlette = 'http://www.neopets.com/prehistoric/omelette.phtml';
    my $response = $browser->get($omlette, @web_headers);
    $html = $response->content;
    $html =~ /<input type\=\'submit\' value\=\'(Grab some Omelette)\'>/;
    $Omlette_OK = $1;
    if ($Omlette_OK) {
        my ($content, $message, $is_success) = do_POST(
        'http://www.neopets.com/prehistoric/omelette.phtml',
        [ type => "get_omelette" ],
    );
    open(OUT, ">/home/oldusername/neopets/omlette-$Date.html") || die $!;
    print OUT $content;
    close(OUT);

    }
}


sub the_Shrine {
    # Approaches the shrine and gets a prize
    my ($content, $message, $is_success) = do_POST(
    'http://www.neopets.com/desert/shrine.phtml',
    [ type => "approach" ],
    );
    open(OUT, ">/home/oldusername/neopets/shrine-$Date.html") || die $!;
    print OUT $content;
    close(OUT);

}


sub do_POST {
    $browser = LWP::UserAgent->new() unless $browser;
    my $resp = $browser->post(@_, @web_headers);
    return ($resp->content, $resp->status_line, $resp->is_success, $resp) if wantarray;
    return unless $resp->is_success;
    return $resp->content;
}

sub do_GET {
    $browser = LWP::UserAgent->new() unless $browser;
    my $resp = $browser->get(@_, @web_headers);
    return ($resp->content, $resp->status_line, $resp->is_success, $resp) if wantarray;
    return unless $resp->is_success;
    return $resp->content;
}
    
