#!/usr/bin/perl -w

# Check what I've won on the wheel of excitement
foreach $file (<wheel*.html>) {
   open(DATA, "$file") || die "cannot open $file: $!\n"; 
   # while ( defined ($html = <DATA>)) {
   while (<DATA>) {
       # if ($html =~ m/<input type\=\'hidden\' name\=\'prize_str\' value\=\'(\w.*)\'>/) {
       if (m/<input type\=\'hidden\' name\=\'prize_str\' value\=\'(\w.*)\'>/) {
           $prize = $1;
           print "$prize \n";
       }
    }
    close (DATA);
}

